<?php
/*
Plugin Name: User Notizblock
Plugin URI: http://allions.net
Description: Ein Notizblock fuer User
Version: 1.0
Author: Alexander Konrad
Author URI: http://allions.net
License:  GPL2
*/

// Dummy file for the plugin, this doesnt really do much at this point apart from including the widget

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once( dirname( __FILE__ ) . '/Widget/user-notizblock-widget.php' );


define('WP_DEBUG',true);
define('WP_DEBUG_DISPLAY',true);
define('WP_DEBUG_LOG',true);

if(!class_exists('UserNotizblock')){

class UserNotizblock{

  var $widget = '';

  public function __construct(){
    $this->widget = new UserNotizblock_Widget();
    $this->widget->initialize();
    add_action('wp_ajax_update_user_note',array($this,'UpdateUserNote'));
    add_option('ubnw_note','0');

  }

  public static function GetCurrentUserNote(){
    if(is_user_logged_in()){
      global $wpdb;
      $mainTableName = "{$wpdb->prefix}usernotes";
      $userTableName = "{$wpdb->prefix}users";
      $uid = get_current_user_id();

      // check if table is in database
      if($wpdb->get_var("SHOW TABLES LIKE '$mainTableName'") != $mainTableName){
        // nope, table not in database!
        // create!
        $charset_collate = $wpdb->get_charset_collate();
        $query = "CREATE TABLE $mainTableName (id bigint(20), note TEXT, UNIQUE KEY id (id)) $charset_collate;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($query);
      }

      $r = $wpdb->get_var("SELECT `note` FROM `$mainTableName` WHERE `id` = $uid");
      if($r!=NULL){
        return $r;
      }
    }
    return "";
  }

  public function UpdateUserNote(){

    if(check_ajax_referer('platform_security','security')){
      if(is_user_logged_in()){
        global $wpdb;
        $sanitizedNote = sanitize_text_field($_POST['note_value']);
        $mainTableName = "{$wpdb->prefix}usernotes";
        $userTableName = "{$wpdb->prefix}users";
        // check if table is in database
        if($wpdb->get_var("SHOW TABLES LIKE '$mainTableName'") != $mainTableName){
          // nope, table not in database!
          // create!
          $charset_collate = $wpdb->get_charset_collate();
          $query = "CREATE TABLE $mainTableName (id bigint(20), note TEXT, UNIQUE KEY id (id)) $charset_collate;";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($query);
        }

        // check if there already is an entry for the current users
        $uid = get_current_user_id();

        $wpdb->replace($mainTableName, array('id'=>$uid,'note'=>$sanitizedNote),  array('%d','%s'));
      }
    }
  }
}

$instance = new UserNotizblock();

}

?>
