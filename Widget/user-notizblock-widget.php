<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class UserNotizblock_Widget extends WP_Widget{

  public function __construct()
  {
    parent::__construct("usrNotizBlock", "User Notizblock");
  }

  public function initialize(){
    add_action("widgets_init", array($this,'RegisterNecessaryHooks'));

  }

  public function RegisterNecessaryHooks(){
    // add css
    wp_enqueue_style('usrNotizBlock',plugins_url("css/user-notizblock-widget.css",__FILE__), array(), '20170515');
    // add jquery
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-core');

    // add js
    wp_register_script( 'usrNotizBlock', plugins_url( "js/user-notizblock-widget.js", __FILE__ ), array(), '20170515' );
    // make the plugin root path available to javascript
    // register ajax hook for text submit
    $ubnw_js_variable_db = array(
      'widget_ajax_url' => admin_url( 'admin-ajax.php' ),
      'ajax_nonce' => wp_create_nonce('platform_security')
    );
    wp_localize_script('usrNotizBlock','ubnw_js_variable_db',$ubnw_js_variable_db);
    wp_enqueue_script('usrNotizBlock');

    // register widget
    register_widget('UserNotizblock_Widget');

  }

  // The Admin UI form used to display this object
  public function form($instance){
    include plugin_dir_path(__FILE__).'/php/form.html';
  }

  public function widget($args, $instance){
    include plugin_dir_path(__FILE__).'/php/base-widget.php';

  }



}

?>
