<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' ); ?>

<div class=unbw_Container>
  <?php if ( is_user_logged_in() ) { ?>
      <textarea  name="t" rows="10" cols="50" class="unbw_Textarea" id="unbw_Textarea" name="unbw_Textarea"><?php echo UserNotizBlock::GetCurrentUserNote() ?></textarea>

      <input type="button" id="unbw_confirm" class="unbw_submitButton" value="confirm">
      <p>
      <a href="<?php echo wp_logout_url(); ?>">Logout</a>
      </p>
  <?php }
  else { ?>
      <a href="<?php echo wp_login_url(); ?>" title="Members Area Login" rel="home">Login</a>
  <?php } ?>
</div>
