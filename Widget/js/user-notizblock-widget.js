jQuery(document).ready(function($){
  // bind the on click event for the confirm button
  jQuery('#unbw_confirm').click(function(){
    var ajax_data = {
      'action' : 'update_user_note',
      'note_value' : jQuery('#unbw_Textarea').val(),
      'security' : ubnw_js_variable_db.ajax_nonce
    };
    jQuery.post(ubnw_js_variable_db.widget_ajax_url,ajax_data,function(response){
      if(response == 0){
        alert("Note saved!");
      }else{
        alert("Failure saving note...");
      }
    });
  })
});
